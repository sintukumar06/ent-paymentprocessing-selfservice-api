package com.stateauto.logging;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.config.Node;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.filter.AbstractFilter;
import org.apache.logging.log4j.message.Message;

@Plugin(name = "DenyGetTransactionsByTypeResponse", category = Node.CATEGORY, elementType = Filter.ELEMENT_TYPE)
public class DenyGetTransactionsByTypeResponse extends AbstractFilter {

	private DenyGetTransactionsByTypeResponse(Result onMatch, Result onMismatch) {
		super(onMatch, onMismatch);
	}

	@Override
	public Result filter(LogEvent event) {
		return this.filter(event.getLoggerName(), event.getMessage().getFormattedMessage());
	}

	@Override
	public Result filter(Logger logger, Level level, Marker marker, Message msg, Throwable t) {
		return msg == null ? this.onMismatch : this.filter(logger.getName(), msg.getFormattedMessage());
	}

	@Override
	public Result filter(Logger logger, Level level, Marker marker, Object msg, Throwable t) {
		return msg == null ? this.onMismatch : this.filter(logger.getName(), msg.toString());
	}

	@Override
	public Result filter(Logger logger, Level level, Marker marker, String msg, Object... params) {
		return this.filter(logger.getName(), msg);
	}

	private Result filter(String logName, String msg) {
		if (msg == null)
			return onMatch;

		if (logName.equals("org.mule.module.http.internal.HttpMessageLogger")) {
			if (msg.contains("REQUESTER")) {
				if (msg.contains("{\"transactions\"") || msg.contains("\"postedAmount\"")
						|| msg.contains("\"customerName\"")) {
					return onMatch;
				}
			}
		}
		return onMismatch;
	}

	@PluginFactory
	public static DenyGetTransactionsByTypeResponse createFilter() {
		return new DenyGetTransactionsByTypeResponse(Result.DENY, Result.NEUTRAL);
	}
}
