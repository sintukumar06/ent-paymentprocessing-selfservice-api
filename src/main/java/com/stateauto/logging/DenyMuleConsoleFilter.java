package com.stateauto.logging;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.config.Node;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.filter.AbstractFilter;
import org.apache.logging.log4j.message.Message;

@Plugin(name = "DenyMuleConsoleFilter", category = Node.CATEGORY, elementType = Filter.ELEMENT_TYPE)
public class DenyMuleConsoleFilter extends AbstractFilter {

	private DenyMuleConsoleFilter(Result onMatch, Result onMismatch) {
		super(onMatch, onMismatch);
	}

	@Override
	public Result filter(LogEvent event) {		
		// format: just the placeholder like "Hello {}"
		// message: what's put into the format like "World"
		return this.filter(event.getLoggerName(), event.getMessage().getFormattedMessage());
	}

	@Override
	public Result filter(Logger logger, Level level, Marker marker, Message msg, Throwable t) {
		return msg == null ? this.onMismatch : this.filter(logger.getName(), msg.getFormattedMessage());
	}

	@Override
	public Result filter(Logger logger, Level level, Marker marker, Object msg, Throwable t) {
		return msg == null ? this.onMismatch : this.filter(logger.getName(), msg.toString());
	}

	@Override
	public Result filter(Logger logger, Level level, Marker marker, String msg, Object... params) {
		return this.filter(logger.getName(), msg);
	}

	private Result filter(String logName, String msg) {
		if (msg == null)
			return onMatch;

		if (logName.equals("org.mule.module.http.internal.HttpMessageLogger"))
		{
		if (msg.contains("LISTENER")) {
			if (msg.contains("Content-Type: text/css") || msg.contains("Content-Type: text/html")
					|| msg.contains("Content-Type: application/raml+yaml")
					|| msg.contains("Accept: application/raml+yaml") || msg.contains("selfservice/2.0/console/")
					|| msg.contains("Content-Type: application/octet-stream") || msg.contains("Ping: OK")
					|| msg.contains("Content-Type: application/x-javascript") || msg.contains("__ping__")) {
				return onMatch;
			}
		}
		}
		return onMismatch;

	}

	@PluginFactory
	public static DenyMuleConsoleFilter createFilter() {
		return new DenyMuleConsoleFilter(Result.DENY, Result.NEUTRAL);
	}
}
