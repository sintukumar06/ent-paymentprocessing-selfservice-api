package com.stateauto.logging;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.pattern.ConverterKeys;
import org.apache.logging.log4j.core.pattern.LogEventPatternConverter;
import org.apache.logging.log4j.core.pattern.MessagePatternConverter;

@Plugin(name = "LogMaskingConverter", category = "Converter")
@ConverterKeys(value = { "masked" })
public class LogMaskingConverter extends LogEventPatternConverter {
	private static final String SOAP_API_PASSWORD_REGX = "<soap:password>.+?</soap:password>";
	private static final Pattern SOAP_API_PASSWORD_PATTERN = Pattern.compile(SOAP_API_PASSWORD_REGX);
	private static final String SOAP_API_PASSWORD_REPLACEMENT = "<soap:password>...</soap:password>";

	private static final String AUTH_KEY_REGX = "((-[a-f0-9]{4}){4}[a-f0-9]{8})";
	private static final Pattern AUTH_KEY_PATTERN = Pattern.compile(AUTH_KEY_REGX);
	private static final String AUTH_KEY_REPLACEMENT = "-xxxx-xxxx-xxxx-xxxxxxxxxxxx";

	private static final String NAME = "masked";

	public LogMaskingConverter() {
		super(NAME, Thread.currentThread().getName());
	}

	public static LogMaskingConverter newInstance(final Configuration config, final String[] options) {
		return new LogMaskingConverter();
	}

	@Override
	public void format(LogEvent event, StringBuilder outputMessage) {
		String message = event.getMessage().getFormattedMessage();
		String maskedMessage = message;
		try {
			maskedMessage = mask(message);
		} catch (Exception e) {
			System.out.println("Failed While Masking");
			maskedMessage = message;
		}
		outputMessage.append(maskedMessage);

	}

	private String mask(String message) {
		Matcher matcher = null;
		StringBuffer buffer = new StringBuffer();

		matcher = SOAP_API_PASSWORD_PATTERN.matcher(message);
		maskMatcher(matcher, buffer, SOAP_API_PASSWORD_REPLACEMENT);
		message = buffer.toString();
		buffer.setLength(0);

		matcher = AUTH_KEY_PATTERN.matcher(message);
		maskMatcher(matcher, buffer, AUTH_KEY_REPLACEMENT);

		return buffer.toString();
	}

	private StringBuffer maskMatcher(Matcher matcher, StringBuffer buffer, String maskStr) {
		while (matcher.find()) {
			matcher.appendReplacement(buffer, maskStr);
		}
		matcher.appendTail(buffer);
		return buffer;
	}
}