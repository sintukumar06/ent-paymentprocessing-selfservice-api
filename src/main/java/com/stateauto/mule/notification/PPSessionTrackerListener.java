package com.stateauto.mule.notification;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.MDC;
import org.mule.extension.http.api.HttpRequestAttributes;
import org.mule.runtime.api.event.Event;
import org.mule.runtime.api.metadata.TypedValue;
import org.mule.runtime.api.notification.PipelineMessageNotification;
import org.mule.runtime.api.notification.PipelineMessageNotificationListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PPSessionTrackerListener implements PipelineMessageNotificationListener<PipelineMessageNotification> {
	private static Logger log = LoggerFactory.getLogger(PPSessionTrackerListener.class);

	private static boolean _active = true;

	private static final String _FLOWNAME = "flowName";

	private final int _MAX_STRING = 16;

	private static String _COLON = ":";

	private static String _CID = "CID";

	private static String _SSID = "SA-SourceSystemCID";

	private static String _DASH = "-";

	private List<String> headers;

	private String appName;

	public void setActive(boolean isActive) {
		_active = isActive;
	}

	boolean isActive() {
		return _active;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public void setHeaders(List<String> headers) {
		this.headers = headers;
	}

	@Override
	public void onNotification(PipelineMessageNotification notification) {
		if (notification.getAction().getActionId() == PipelineMessageNotification.PROCESS_START) {
			final Event event = notification.getEvent();
			TypedValue cid = event.getVariables().get(PPSessionTrackerListener._CID);
			if (cid == null) {
				String cidString = "pp" + _COLON + event.getCorrelationId().substring(0, 7); //
				MDC.put(_CID, cidString);
				//event.getVariables().put(PPSessionTrackerListener._CID, TypedValue.of(cidString));

				TypedValue<HttpRequestAttributes> attributes = event.getMessage().getAttributes();
				if (attributes != null && attributes.getValue() != null) {
					Map headers = attributes.getValue().getHeaders();
				}
				// (PPSessionTrackerListener._SSID);
				// if
				// (saSourceSystemCid != null && !StringUtils.isEmpty(saSourceSystemCid)) { cid
				// * = saSourceSystemCid.replace(",", "").replace(" ", "") + _DASH + cid; }
				// * event.getVariables().put(PPSessionTrackerListener._CID,
				// TypedValue.of(cid));
				// *
				/*
				 * if (headers != null && !headers.isEmpty()) { // Put headers listed as
				 * property for (String header : headers) { String headerValue =
				 * event.getMessage().getInboundProperty(header); if (headerValue != null &&
				 * !StringUtils.isEmpty(headerValue)) { event.setSessionVariable(header,
				 * headerValue); } } }
				 */

			}
		}
	}
}
