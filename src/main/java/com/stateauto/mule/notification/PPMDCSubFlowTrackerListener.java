package com.stateauto.mule.notification;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
//import org.mule.DefaultMuleEvent;
//import org.mule.api.context.notification.MessageProcessorNotificationListener;
import org.mule.runtime.api.notification.*;
//import org.mule.api.processor.LoggerMessageProcessor;
//import org.mule.context.notification.MessageProcessorNotification;
//import org.mule.util.StringUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

public class PPMDCSubFlowTrackerListener implements MessageProcessorNotificationListener<MessageProcessorNotification> {

	private static final String _CID = "CID";

	private static final String _FLOWNAME = "flowName";

	private List<String> headers;

	public void setHeaders(List<String> headers) {
		this.headers = headers;
	}

	private PPSessionTrackerListener ppSessionTrackerListener;

	@Autowired
	public void setPPSessionTrackerListener(PPSessionTrackerListener ppSessionTrackerListener) {
		this.ppSessionTrackerListener = ppSessionTrackerListener;
	}

	@Override
	public void onNotification(MessageProcessorNotification notification) {
		// if (this.ppSessionTrackerListener.isActive()) {
		// if (notification.getAction().getActionId() ==
		// MessageProcessorNotification.MESSAGE_PROCESSOR_PRE_INVOKE) {
		// // if (notification.getProcessor().getClass() ==
		// LoggerMessageProcessor.class) {
		// if (notification.getEvent(). == LoggerMessageProcessor.class) {
		// final DefaultMuleEvent event = (DefaultMuleEvent) notification.getSource();
		// // CID
		// final String cid = event.getSessionVariable(_CID);
		// if (cid != null) {
		// MDC.put(_CID, cid);
		// }
		//
		// // Headers
		// if (headers != null && !headers.isEmpty()) {
		// Put headers listed as property
		// for (String header : headers) {
		// String headerValue = event.getSessionVariable(header);
		// if (headerValue != null && !StringUtils.isEmpty(headerValue)) {
		// MDC.put(header, headerValue);
		// continue;
		// }
		// headerValue = event.getFlowVariable(header);
		// if (headerValue != null && !StringUtils.isEmpty(headerValue)) {
		// MDC.put(header, headerValue);
		// }
		// }
		// }
		//
		// String flowName = (String) event.getMuleContext().getExpressionManager()
		// .parse("#[context:serviceName]", event);
		// if (flowName != null && !StringUtils.isEmpty(flowName)) {
		// event.setFlowVariable(_FLOWNAME, flowName);
		// MDC.put(_FLOWNAME, flowName);
		// }
		// }
		// } else if (notification.getAction().getActionId() ==
		// MessageProcessorNotification.MESSAGE_PROCESSOR_POST_INVOKE) {
		// MDC.clear();
		// }
		// }
	}
}
