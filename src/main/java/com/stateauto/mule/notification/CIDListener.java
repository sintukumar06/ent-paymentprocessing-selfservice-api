package com.stateauto.mule.notification;

import java.util.concurrent.TimeUnit;

import org.mule.extension.http.api.HttpRequestAttributes;
import org.mule.runtime.api.event.Event;
import org.mule.runtime.api.metadata.TypedValue;
import org.mule.runtime.api.notification.PipelineMessageNotification;
import org.mule.runtime.api.notification.PipelineMessageNotificationListener;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class CIDListener implements PipelineMessageNotificationListener<PipelineMessageNotification> {

	private static LoadingCache<String, String> cache = CacheBuilder.newBuilder().expireAfterWrite(2, TimeUnit.MINUTES)
			.build(new CacheLoader<String, String>() {
				@Override
				public String load(String key) {
					return key;
				}
			});

	@Override
	public void onNotification(PipelineMessageNotification notification) {
		if (notification.getAction().getActionId() == PipelineMessageNotification.PROCESS_START) {
			Event event = notification.getEvent();
			String cid = "pp:" + ((String) event.getCorrelationId()).substring(0, 7); // TODO: read from config
			TypedValue<HttpRequestAttributes> req = event.getMessage().getAttributes();
			String saSourcesystemcid = req.getValue().getHeaders().get("SA-SourceSystemCID");
			if (saSourcesystemcid != null) {
				cid += "-" + saSourcesystemcid;
			}
			cache.put(event.getCorrelationId(), cid);
		}
	}

	public static String toCID(String correlationId) {
		return cache.getIfPresent(correlationId);
	}
}