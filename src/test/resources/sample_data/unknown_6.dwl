<?xml version="1.0"?>
<tns:Envelope xmlns:tns="http://schemas.xmlsoap.org/soap/envelope/">
  <tns:Body>
    <getBillingDetailResponse xmlns="http://example.com/com/stateauto/integration/paymentprocessing/BCPaymentProcessingAPI">
      <return xmlns:pogo="http://example.com/com/stateauto/integration/paymentprocessing">
        <pogo:AccountGroupCode>PL</pogo:AccountGroupCode>
        <pogo:AccountInvoices/>
        <pogo:AccountNumber>0011400013</pogo:AccountNumber>
        <pogo:ChargeFee>false</pogo:ChargeFee>
        <pogo:DisplayMessage>Your account is paid in full.</pogo:DisplayMessage>
        <pogo:DisplayPastDueOnly>false</pogo:DisplayPastDueOnly>
        <pogo:InsuredAddress>5305 SCHULER ST</pogo:InsuredAddress>
        <pogo:InsuredCity>HOUSTON</pogo:InsuredCity>
        <pogo:InsuredName>ALISHA TX</pogo:InsuredName>
        <pogo:InsuredPhoneNumber>614-444-5555</pogo:InsuredPhoneNumber>
        <pogo:InsuredState>TX</pogo:InsuredState>
        <pogo:InsuredZip>77007</pogo:InsuredZip>
        <pogo:InvoiceDayOfMonth>20</pogo:InvoiceDayOfMonth>
        <pogo:IsInvoiceDueDateOfMonthEditable>true</pogo:IsInvoiceDueDateOfMonthEditable>
        <pogo:PastDueAmount>0.00</pogo:PastDueAmount>
        <pogo:PaymentAmount>0.00</pogo:PaymentAmount>
        <pogo:PaymentInstruments/>
        <pogo:PaymentWithdrawalDate/>
        <pogo:ReferenceID>SSL1-4-5-ebdf7c6f</pogo:ReferenceID>
        <pogo:RemainingBalance>0.00</pogo:RemainingBalance>
        <pogo:WithdrawalInProcess>false</pogo:WithdrawalInProcess>
      </return>
    </getBillingDetailResponse>
  </tns:Body>
</tns:Envelope>